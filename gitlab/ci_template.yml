stages:
  - 'preparation'
  # Test just one basic build, if it succeeds proceed to test the rest
  - 'build'
  - 'test'
  # Run multiple builds and tests, multi-distro, multi-arch
  - 'full builds'
  - 'full tests'

variables:
  GIT_STRATEGY: none
  DEFAULT_MESON_ARGS: >
    --werror
    -Dpython=enabled
    -Dlibav=enabled
    -Dugly=enabled
    -Dbad=enabled
    -Ddevtools=enabled
    -Dges=enabled
    -Drtsp_server=enabled
    -Dvaapi=enabled
    -Dsharp=disabled

manifest:
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/build-manifest:f085d34a32cb8cac95cbd420344efeaa647d08d5'
  stage: 'preparation'
  script:
    - cd /gst-ci
    - gitlab/build_manifest.py --self-update
    - gitlab/build_manifest.py ${CI_PROJECT_DIR}/manifest.xml
    - cat ${CI_PROJECT_DIR}/manifest.xml
  artifacts:
    expire_in: "7 days"
    paths:
      - "manifest.xml"

gst indent:
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/gst-indent:6f7e01e1e30a73efa880acdc8e911f1f20c58dbb'
  stage: 'preparation'
  variables:
    GIT_STRATEGY: 'fetch'
  script:
    # man indent. grep RETURN VALUE, grab a beer on my behalf...
    - indent --version || true
    - curl -o gst-indent https://gitlab.freedesktop.org/gstreamer/gstreamer/raw/master/tools/gst-indent
    - chmod +x gst-indent
    - find . -name '*.c' -exec ./gst-indent {} +
    - |
      if git diff --quiet; then
          echo "Code is properly formatted"
      else
          git diff --color=always
          echo 'style diverges, please run gst-indent first'
          exit 1
      fi
  except:
    variables:
      # No point on trying to format C files in those repositories
      - $CI_PROJECT_NAME == "gstreamer-sharp"
      - $CI_PROJECT_NAME == "cerbero"

.build:
  stage: 'full builds'
  dependencies:
    - "manifest"
  variables:
    CC: "ccache gcc"
    CXX: "ccache g++"
    CCACHE_BASEDIR: "${CI_PROJECT_DIR}"
    CCACHE_DIR: "${CI_PROJECT_DIR}/ccache"
    MAIN_DIR: "${CI_PROJECT_DIR}/validate-output/"
    MESON_ARGS: "${DEFAULT_MESON_ARGS}"

  script:
    - ccache -z
    # Not sure why, but permission errors else
    # https://gitlab.freedesktop.org/alatiera/gstreamer/-/jobs/41441
    - cp -r /gst-build/ . && cd gst-build
    - ./git-update --no-interaction --manifest="${CI_PROJECT_DIR}/manifest.xml"
    - meson build/ $MESON_ARGS
    - ninja -C build/
    - ccache -s
  after_script:
    - cd gst-build/
    # Clean the artifacts packages to avoid copying "useless" build products.
    - test -d build && find build -name '*.[ao]' -delete
    # Clean the .git repos since we won't need them anymore
    - rm -rf subprojects/*/.git/
    - rm -rf build/subprojects/*/.git/
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - "${CCACHE_DIR}"
  artifacts:
    expire_in: '5 days'
    when: always
    paths:
      - "manifest.xml"
      - "gst-build/"
  except:
    variables:
      - $CI_PROJECT_NAME == "cerbero"

build fedora x86_64:
  extends: '.build'
  stage: 'build'
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/fedora:6f7e01e1e30a73efa880acdc8e911f1f20c58dbb'

build android arm64 api28:
  extends: '.build'
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/ubuntu:aa13631e38cb4e1e4ee1c869aa523952dcd8cca7'
  variables:
    MESON_ARGS: >
      -Dbad=enabled
      -Dbad:androidmedia=enabled
      --cross-file cross-files/android_arm64_api28.txt

.test:
  stage: 'test'
  variables:
    MAIN_DIR: "${CI_PROJECT_DIR}/validate-output/"
    # Disable colored output to avoid weird rendering issues
    GST_DEBUG_COLOR: 'no'

    # note the -b at the start of each line
    # Can't comment inline sadly
    # FIXME: get this into gst-validate-launcher KNOWN_ISSUES
    # elements_multisocketsink.test_sending_buffers_with_9_gstmemories: https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/525
    # elements_multisocketsink.test_client_next_keyframe: https://gitlab.freedesktop.org/gstreamer/gst-plugins-base/issues/516
    # flvmux.test_incrementing_timestamps: https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/530
    # rtpbin.test_sender_eos: https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/527
    # souphttpsrc.test_icy_stream: https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/307
    # rtspserver.test_shared_tcp: https://gitlab.freedesktop.org/gstreamer/gst-rtsp-server/issues/54
    # dtls: https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/issues/811
    # mpegtsmux.test_align: https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/issues/844
    # shm.test_shm_live: https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/issues/847
    # splitmux.test_splitmuxsink_async: https://gitlab.freedesktop.org/gstreamer/gst-plugins-good/issues/531
    BLACKLIST: >
      -b check.gst-plugins-base.elements_multisocketsink.test_sending_buffers_with_9_gstmemories
      -b check.gst-plugins-base.elements_multisocketsink.test_client_next_keyframe
      -b check.gst-plugins-good.elements_souphttpsrc.test_icy_stream
      -b check.gst-plugins-good.elements_rtpbin.test_sender_eos
      -b check.gst-plugins-good.elements_flvmux.test_incrementing_timestamps
      -b check.gst-plugins-bad.elements_dtls.test_data_transfer
      -b check.gst-plugins-bad.elements_dtls.test_create_and_unref
      -b check.gst-rtsp-server.gst_rtspserver.test_shared_tcp
      -b check.gst-plugins-bad.elements_shm.test_shm_live
      -b check.gst-plugins-good.elements_splitmux.test_splitmuxsink_async
  script:
    - cd gst-build/
    - >
      ./gst-uninstalled.py
      gst-validate-launcher ${TEST_SUITE}
      -fs
      -j=1
      --mute
      --shuffle
      --no-display
      --dump-on-failure
      --meson-no-rebuild
      -M "${CI_PROJECT_DIR}/validate-output/"
      --xunit-file "${CI_PROJECT_DIR}/validate-output/logs/xunit.xml"
      ${BLACKLIST}
  artifacts:
    expire_in: '14 days'
    when: always
    paths:
      - 'gst-build/build/meson-logs/'
      - 'validate-output/logs'
    reports:
      junit:
        - "validate-output/logs/*.xml"
  # We disable the .build above, which this job usually depends upon for cerbero
  except:
    variables:
      - $CI_PROJECT_NAME == "cerbero"

.test fedora x86_64:
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/fedora:6f7e01e1e30a73efa880acdc8e911f1f20c58dbb'
  extends: '.test'
  dependencies:
    - build fedora x86_64

check fedora:
  extends: '.test fedora x86_64'
  variables:
    TEST_SUITE: "check.gst*"

# Template for Cerbero GStreamer Build
#
# Parameters:
# CONFIG: The name of the configuration file to use
# ARCH: The cerbero <distro>_<cpu> (used in cache key)
#
# Produces runtime and devel tarball packages.
.cerbero:
  stage: "full builds"
  dependencies:
    - "manifest"
  variables:
    CCACHE_BASEDIR: "${CI_PROJECT_DIR}"
    CCACHE_DIR: "${CI_PROJECT_DIR}/ccache"
    CCACHE_MAXSIZE: "1.7G"
    CERBERO_HOME: "${CI_PROJECT_DIR}/cerbero-build"
    CERBERO_SOURCES: "${CI_PROJECT_DIR}/cerbero-sources"
    CERBERO_DEPS: "cerbero-deps.tar.gz"
    CERBERO: ./cerbero-uninstalled -c config/${CONFIG} -c localconf.cbc -m manifest.xml
  before_script:
    # FIXME Wrong namespace
    # Workaround build-tools having hardcoded internal path 
    - mkdir -p /builds/gstreamer
    - ln -sf ${CI_PROJECT_DIR} /builds/gstreamer/cerbero
    - rsync -aH /cerbero/ .
    - mkdir -p ${CERBERO_HOME}
    - test -f ${CERBERO_DEPS} && tar -C ${CERBERO_HOME} -xf ${CERBERO_DEPS}
    - echo "home_dir = \"${CERBERO_HOME}\"" >> localconf.cbc
    - echo "local_sources = \"${CERBERO_SOURCES}\"" >> localconf.cbc
    - ./cerbero-uninstalled --self-update manifest.xml
    - ccache -z
  script:
    - $CERBERO show-config
    - $CERBERO fetch-bootstrap --build-tools-only
    - $CERBERO fetch-package --deps gstreamer-1.0
    - $CERBERO fetch-cache
    - $CERBERO bootstrap --offline --build-tools-only
    - $CERBERO package --offline -t -o ${CI_PROJECT_DIR} gstreamer-1.0
    - ccache -s
  cache:
    key: "cerbero_${ARCH}"
    paths:
      - "${CCACHE_DIR}"
      - "${CERBERO_SOURCES}"
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_SHA}"
    expire_in: '5 days'
    when: 'always'
    paths:
      - "manifest.xml"
      - "cerbero-build/logs"
      - "*.tar.bz2"

# Template for Cerbero GStreamer Deps
#
# This template is used by cerbero/ project to pre-built the GStreamer
# depedencies. When available, the .cerbero jobs will download this artifact
# in order to speed up the build.
#
# Parameters:
# CONFIG: The name of the configuration file to use
# ARCH: The architecture name (Cerbero naming)
#
# Produce an artifact with the dist/ and .cache for the ARCH along
# with the associated build-tools.
.cerbero deps:
  extends: .cerbero
  stage: "build"
  script:
    - $CERBERO show-config
    - $CERBERO fetch-bootstrap --build-tools-only
    - $CERBERO fetch-package --deps gstreamer-1.0
    - $CERBERO bootstrap --offline --build-tools-only
    - $CERBERO build-deps --offline
          gstreamer-1.0 gst-plugins-base-1.0 gst-plugins-good-1.0
          gst-plugins-bad-1.0 gst-plugins-ugly-1.0 gst-rtsp-server-1.0
          gst-libav-1.0 gst-validate gst-editing-services-1.0 libnice
    - $CERBERO fetch-cache --skip-fetch --job-id=${CI_JOB_ID}
    - tar -C ${CERBERO_HOME} -czf $CERBERO_DEPS
              build-tools build-tools.cache
              dist/${ARCH} ${ARCH}.cache
    - ccache -s
  only:
    variables:
      - $CI_PROJECT_NAME == "cerbero"
  artifacts:
    name: "${CI_JOB_NAME}_${CI_COMMIT_SHA}"
    expire_in: '10 days'
    when: 'always'
    paths:
      - "manifest.xml"
      - "cerbero-build/logs"
      - "cerbero-build/cerbero-deps.log"
      - "${CERBERO_DEPS}"

.build cerbero fedora x86_64:
  extends: '.cerbero'
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/cerbero-fedora:d19082b72667fb3382bdc3621520c4d26e258b2e'
  variables:
    CONFIG: "linux.config"
    ARCH: "linux_x86_64"

.cerbero deps fedora x86_64:
  extends: '.cerbero deps'
  image: 'registry.freedesktop.org/gstreamer/gst-ci/amd64/cerbero-fedora:d19082b72667fb3382bdc3621520c4d26e258b2e'
  variables:
    CONFIG: "linux.config"
    ARCH: "linux_x86_64"

# This indirection allow overriding the image without passing again CONFIG and ARCH
build cerbero fedora x86_64:
  extends: '.build cerbero fedora x86_64'
